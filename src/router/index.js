import Vue from 'vue'
import Router from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios,axios);

import HelloWorld from '@/components/HelloWorld'
import ListUser from '@/components/ListUser'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/listuser',
      name: 'ListUser',
      component: ListUser
    }
  ]
})
